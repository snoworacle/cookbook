# pukkamustard's Hummus

Ingredients:
- Chickpeas
- Tahini paste
- Olive oil
- Garlic
- Salt
- Natron
- Pepper and spices

1. Take a bunch of chickpeas and soak them overnight.
2a) Bring chickpeas and Natron in plenty of water to a boil
2b) Simmer the chickpeas in water until they are creamy soft but still have a slight bite (about 2-3 hours).
3. Drain extra water and keep a couple of chickpeas aside as garnish.
4. Add to the chickpeas:
  - Tahini past (enough that you can taste the sesame)
  - Garlic cloves (be liberal)
  - Olive oil (plenty)
  - Salt (enough so that the Hummus can be eaten on its own with bread)
  - Pepper (preferably from Patima's garden)
  - Paprika (not only for flavor but also color)
5. Blend it. Make sure garlic cloves are ground up but don't grind the chickpeas too fine.
6. Add water and mix into the paste until you get a nice creamy but coarse texture.
7. Garnish with chickpeas, olive oil and spices.

Enjoy the Hummus with bread, naan or chapati on a nice sunny day by the lake in Zurich. Hummus can also be traded for tax declarations, biscuits and other delicious treats.
